import Overlay from './Overlay.svelte'

const app = new Overlay({
  target: document.getElementById('app')!,
})

export default app
