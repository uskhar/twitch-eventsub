/**
 * Parses data passed after a hash in the URL into a javascript object. 
 */
export function getHashData(): Record<string, string> {
  const hash = window.location.hash;
  if (hash) {
    const hashElements = hash.slice(1).split("&");
    const hashData = Object.fromEntries(hashElements.map((element) => {
      return element.split("=");
    }));
    return hashData
  }
  return {}
}
