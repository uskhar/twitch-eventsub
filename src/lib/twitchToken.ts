
/**
 * Gets the twitch token from localStorage.
 * Returns null if the token is not set.
 */
export function getToken() {
  return localStorage.getItem('twitch-token')
}

/**
 * Sets the twitch token in localStorage.
 */
export function setToken(token: string) {
  localStorage.setItem('twitch-token', token)
}

export function clearToken() {
  localStorage.removeItem('twitch-token')
}

/**
 * Generates a random token for CSRF validation and stores it
 * in a cookie.
 */
export function generateValidationState(): string {
  const state = Math.random().toString()
  document.cookie = `oauth2state=${state}`;
  return state
}

/**
 * Compares saved CSRF validation token with the state
 * provided by twitch.
 * @param state State provided by twitch for comparison
 */
export function validateState(state: string) {
  const storedState = document.cookie
    .split("; ")
    .find((row) => row.startsWith("oauth2state="))
    ?.split("=")[1];
  document.cookie = "oauth2state=; expires=Thu, 01 Jan 1970 00:00:00 GMT"
  return state === storedState
}