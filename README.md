# Twitch EventSub

Example application to display a hydration meter overlay, which decreases over time and can be refilled by claiming a hydration reward through twitch.

Uses the Twitch EventSub API with websockets.

This project uses the [Svelte](https://svelte.dev/docs/introduction) library which mixes HTML, JavaScript and CSS in a pretty simple way in the same file.

We also use TypeScript instead of JavaScript. They're very similar, but typescript uses explicit types to make it easier to discover obvious errors.

## Requirements

- [NodeJS + NPM](https://nodejs.org/en/download)
- [Git](https://git-scm.com/downloads)

## Setup

Open a terminal and run the following commands to download and setup the application.

```bash
git clone https://gitlab.com/uskhar/twitch-eventsub.git
cd twitch-eventsub
npm install
```

## Code suggestions

I suggest you start looking at `src/overlay/Overlay.svelte` and `src/overlay/HydrationMeter.svelte` to start implementing and altering how the overlay works.

## Server and App Client ID

This app is set up with Client ID configured in Uskhar's developer console on https://dev.twitch.tv. It requires the server to be run on http://127.0.0.1:3024 for logins to be valid.

The development server will automatically update the page as soon as you make any changes to the code.

```bash
# Start development server on http://127.0.0.1:3024
npm run dev
``` 

If you want to build the application and serve it locally, you could do it for example with python3 `http.server` module, or any other tool you like.

```bash
# Build the application into the `dist` directory
npm run build
# Start dist server on http://127.0.0.1:3024
python3 -m http.server --bind 127.0.0.1 -d dist 3024
```

## Recommended IDE Setup

[VS Code](https://code.visualstudio.com/) + [Svelte](https://marketplace.visualstudio.com/items?itemName=svelte.svelte-vscode).

## Relevant Twitch API documentation pages

[EventSub Documentation](https://dev.twitch.tv/docs/eventsub/)  
[EventSub Documentation for websockets](https://dev.twitch.tv/docs/eventsub/handling-websocket-events/)  
[EventSub list of available subscriptions](https://dev.twitch.tv/docs/eventsub/eventsub-subscription-types/)  
[EventSub Documentation for reward redemption](https://dev.twitch.tv/docs/eventsub/eventsub-subscription-types/#channelchannel_points_custom_reward_redemptionadd)  
[Twitch API Reference for event subscription](https://dev.twitch.tv/docs/api/reference/#create-eventsub-subscription)  
